﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;

namespace EnsembleMethods
{
    class DiabetesDiagnosis
    {
        private static string TRAINING_DATA_RELATIVE_PATH = @"..\..\Data\diabetes_train.txt";
        private static string TEST_DATA_RELATIVE_PATH = @"..\..\Data\diabetes_test.txt";

        private static string UNKONWN = "?";
        private static int NUM_BOOTSTRAP_SAMPLES = 30;
        private List<Dictionary<string, Attribute>> bootstrapAttrsByName = new List<Dictionary<string, Attribute>>(NUM_BOOTSTRAP_SAMPLES);
        private List<Dictionary<string, Attribute>> consolidatedBootstrapAttrsByName = new List<Dictionary<string, Attribute>>(NUM_BOOTSTRAP_SAMPLES);
        private List<List<string>> examples = new List<List<string>>(600);
        private List<List<List<string>>> bootstrapSamples = new List<List<List<string>>>(NUM_BOOTSTRAP_SAMPLES);
        private List<List<List<string>>> consolidatedBootstrapSamples = new List<List<List<string>>>(NUM_BOOTSTRAP_SAMPLES);
        private string positiveClassVal = "1";
        private string negativeClassVal = "0";
        private Random generator = new Random(0);

        /// <summary>
        /// Aggregate attribute values so we don't have as many categories. Assume attribuetsByName is
        /// already initialized by reading in the training data and the AttributeVal are sorted by the
        /// neumeric values.
        /// </summary>
        private Dictionary<string, Attribute> ConsolidateAttributes(Dictionary<string, Attribute> sourceAttributesByName)
        {
            Dictionary<string, Attribute> adjusted = new Dictionary<string, Attribute>();
            foreach (var rawAttr in sourceAttributesByName)
            {
                adjusted.Add(rawAttr.Key, new Attribute() { AttributeName = rawAttr.Key, AttributeVals = new HashSet<string>() });
                int numRawAttrVals = rawAttr.Value.AttributeVals.Count;
                int numGroups = 5 + 5 * (numRawAttrVals / 50);
                int stepSize = numRawAttrVals / numGroups;
                if (stepSize <= 0) stepSize = 1;

                int curIdx = 0;
                while (curIdx < numRawAttrVals)
                {
                    int lb = curIdx;
                    int hb = curIdx + stepSize;
                    if (hb >= numRawAttrVals) { hb = numRawAttrVals - 1; }
                    string consolidatedAttr = rawAttr.Value.AttributeVals.ElementAt(lb) + "-" + rawAttr.Value.AttributeVals.ElementAt(hb);
                    adjusted[rawAttr.Key].AttributeVals.Add(consolidatedAttr);
                    curIdx = lb + stepSize;
                }
            }
            adjusted["Attribute8"] = sourceAttributesByName["Attribute8"]; // Don't adjust for class attribute
            return adjusted;
        }

        private string MapToConsolidatedAttr(string attrName, string rawValue, Dictionary<string, Attribute> consolidatedAttrs)
        {
            double curExampleVal = double.Parse(rawValue);
            var attrVals = consolidatedAttrs[attrName].AttributeVals;
            string result = UNKONWN;
            foreach (string attrVal in attrVals)
            {
                double[] bounds = Array.ConvertAll(attrVal.Split('-'), double.Parse);
                if (bounds[0] <= curExampleVal && curExampleVal <= bounds[1])
                {
                    result = attrVal;
                    break;
                }
            }
            return result;
        }

        private List<string> MapToConsolidatedInstance(List<string> rawInstance, Dictionary<string, Attribute> consolidatedAttrs)
        {
            // Don't consolidate the class attr, which is at the last idx
            List<string> consolidatedExample = new List<string>();
            for (int i = 0; i < rawInstance.Count - 1; i++)
            {
                string mappedResult = MapToConsolidatedAttr("Attribute" + i, rawInstance[i], consolidatedAttrs);
                consolidatedExample.Add(mappedResult);
            }
            consolidatedExample.Add(rawInstance[rawInstance.Count - 1]);
            return consolidatedExample;
        }

        private void ReadTrainingData()
        {
            // Read raw data
            using (StreamReader sr = File.OpenText(TRAINING_DATA_RELATIVE_PATH))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    List<string> newExample = new List<string>(s.Split(','));
                    examples.Add(newExample);
                }
            }

            // generate bootstrap samples
            for (int i = 0; i < NUM_BOOTSTRAP_SAMPLES; i++)
            {
                bootstrapSamples.Add(new List<List<string>>(examples.Count));
                consolidatedBootstrapSamples.Add(new List<List<string>>(examples.Count));
                bootstrapAttrsByName.Add(new Dictionary<string, Attribute>(examples[0].Count));
                for (int c = 0; c < examples.Count; c++)
                {
                    int sampleIdx = generator.Next(examples.Count);
                    var newExample = examples[sampleIdx];
                    bootstrapSamples[i].Add(newExample);

                    for (int attrIdx = 0; attrIdx < newExample.Count; attrIdx++)
                    {
                        string attrName = "Attribute" + attrIdx;
                        if (!bootstrapAttrsByName[i].ContainsKey(attrName))
                        {
                            Attribute newAttr = new Attribute()
                            {
                                AttributeName = attrName,
                                AttributeVals = new SortedSet<string>(new NumericStringAttributeValsComparer())
                            };
                            bootstrapAttrsByName[i].Add(attrName, newAttr);
                        }
                        bootstrapAttrsByName[i][attrName].AttributeVals.Add(newExample[attrIdx]);
                    }
                }
                var consolidatedResult = ConsolidateAttributes(bootstrapAttrsByName[i]);
                consolidatedBootstrapAttrsByName.Add(consolidatedResult);
                foreach (var bootstrapSample in bootstrapSamples[i])
                {
                    consolidatedBootstrapSamples[i].Add(MapToConsolidatedInstance(bootstrapSample, consolidatedResult));
                }
            }
        }

        private void ClassifierCore()
        {
            int trueResultIdx = consolidatedBootstrapAttrsByName[0].Count - 1;
            int instancesCount = 0;
            // Read in test data
            List<List<string>> rawInstances = new List<List<string>>();
            using (StreamReader sr = File.OpenText(TEST_DATA_RELATIVE_PATH))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    List<string> rawTestInstanceData = new List<string>(s.Split(','));
                    rawInstances.Add(rawTestInstanceData);
                    instancesCount++;
                }
            }

            // Run Classifier (Bagging Module)
            int[] numTreesInBag = { 1, 20};
            int[] maxTreeDepth = { 1, 2, 3 };
            foreach (var numTree in numTreesInBag)
            {
                foreach (var maxDepth in maxTreeDepth)
                {
                    int correctPredictionCounts = 0;
                    int truePositiveCount = 0;
                    string[,] predictionStorage = new string[rawInstances.Count, NUM_BOOTSTRAP_SAMPLES];
                    for (int bootstrapIdx = 0; bootstrapIdx < NUM_BOOTSTRAP_SAMPLES; bootstrapIdx++)
                    {
                        BaggingModule baggingMain = new BaggingModule(
                            consolidatedBootstrapSamples[bootstrapIdx], 
                            new List<Attribute>(consolidatedBootstrapAttrsByName[bootstrapIdx].Values), 
                            numTree, maxDepth);

                        for (int testInstIdx = 0; testInstIdx < rawInstances.Count; testInstIdx++)
                        {
                            var instance = MapToConsolidatedInstance(
                                                rawInstances[testInstIdx], 
                                                consolidatedBootstrapAttrsByName[bootstrapIdx]);
                            string prediction = baggingMain.PredictOutcome(instance);
                            if (prediction == instance[trueResultIdx])
                            {
                                correctPredictionCounts++;
                                if (instance[trueResultIdx] == positiveClassVal)
                                {
                                    truePositiveCount++;
                                }
                            }
                            // Store the prediction for later calculation
                            predictionStorage[testInstIdx, bootstrapIdx] = prediction;
                        }
                    }

                    // Calculate bias and variance
                    int biasCount = 0;
                    int varianceCount = 0;
                    for (int testInstIdx = 0; testInstIdx < rawInstances.Count; testInstIdx++)
                    {
                        int pos = 0;
                        int neg = 0;
                        for (int bootstrapIdx = 0; bootstrapIdx < NUM_BOOTSTRAP_SAMPLES; bootstrapIdx++)
                        {
                            if (predictionStorage[testInstIdx, bootstrapIdx] == positiveClassVal)
                            {
                                pos++;
                            }
                            else if (predictionStorage[testInstIdx, bootstrapIdx] == negativeClassVal)
                            {
                                neg++;
                            }
                        }
                        string mainPrediction = (pos > neg) ? positiveClassVal : negativeClassVal;
                        // Bias = 0 if main prediction is correct, 1 otherwise
                        if (mainPrediction != rawInstances[testInstIdx][trueResultIdx])
                        {
                            biasCount++;
                        }
                        // Variance = Prob(Prediction != Main Prediction)
                        for (int bootstrapIdx = 0; bootstrapIdx < NUM_BOOTSTRAP_SAMPLES; bootstrapIdx++)
                        {
                            if (predictionStorage[testInstIdx, bootstrapIdx] != mainPrediction)
                            {
                                varianceCount++;
                            }
                        }
                    }

                    Console.WriteLine("=== #Trees: {0}, maxDepth:{1} ===", numTree, maxDepth);
                    Console.WriteLine("Total Test Instance: {0}", instancesCount * NUM_BOOTSTRAP_SAMPLES);
                    Console.WriteLine("Correctedly Predicted: {0}", correctPredictionCounts);
                    Console.WriteLine("True Positive count: {0}", truePositiveCount);
                    Console.WriteLine("Bias: {0}", biasCount);
                    Console.WriteLine("Variance: {0}", (double)varianceCount / (instancesCount * NUM_BOOTSTRAP_SAMPLES));
                    Console.WriteLine("Overall Accuracy: {0}", (double)correctPredictionCounts / (instancesCount * NUM_BOOTSTRAP_SAMPLES));
                    Console.WriteLine("============================\n");
                }
            }
        }

        public void Start()
        {
            Console.WriteLine("Reading Training Data...");
            ReadTrainingData();

            Console.WriteLine("Running Against Test Data...");
            ClassifierCore();
        }


        static void Main(string[] args)
        {
            try
            {
                DiabetesDiagnosis main = new DiabetesDiagnosis();
                main.Start();
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("Make sure the Input Data file is at the correct location in relation to the executable. See doc for details");
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Press Enter to Exit ...");
            Console.ReadLine();
        }
    }
}
