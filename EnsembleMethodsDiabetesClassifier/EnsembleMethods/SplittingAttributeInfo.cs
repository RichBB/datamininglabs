﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnsembleMethods
{
    public class SplittingAttributeInfo
    {
        public int AttrIdx { get; set; }
        public Dictionary<string, List<List<string>>> AggrExamplesByAttrVal { get; set; }

        public override string ToString()
        {
            return string.Format("AttrIdx: {0}, UniqueValueCount: {1}", AttrIdx, AggrExamplesByAttrVal.Count);
        }
    }
}
