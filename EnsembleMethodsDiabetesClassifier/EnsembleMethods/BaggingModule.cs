﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnsembleMethods
{
    class BaggingModule
    {
        private Random generator = new Random(0);
        private List<DecisionTreeModule> trees = new List<DecisionTreeModule>();

        public BaggingModule(List<List<string>> examples, List<Attribute> attributes, int numTrees, int maxTreeDepth = int.MaxValue)
        {
            int numExampleToTrainTree = examples.Count;
            for (int nt = 0; nt < numTrees; nt++)
            {
                List<List<string>> trainExample = new List<List<string>>(numExampleToTrainTree);
                for (int nex = 0; nex < numExampleToTrainTree; nex++)
                {
                    int randEntIdx = generator.Next(0, examples.Count);
                    trainExample.Add(examples[randEntIdx]);
                }
                trees.Add(new DecisionTreeModule(trainExample, attributes, 1.0, maxTreeDepth));
            }
        }

        public string PredictOutcome(List<string> toPredict)
        {
            Dictionary<string, int> tally = new Dictionary<string, int>();
            foreach (var classifier in trees)
            {
                string predictionTrace = "";
                string prediction = classifier.PredictOutcome(toPredict, out predictionTrace);
                if (!tally.ContainsKey(prediction))
                {
                    tally.Add(prediction, 1);
                }
                else
                {
                    tally[prediction] = tally[prediction] + 1;
                }
            }

            string result = "";
            int maxTallyCount = -1;
            foreach (var tallyEnt in tally)
            {
                if (tallyEnt.Value > maxTallyCount)
                {
                    result = tallyEnt.Key;
                    maxTallyCount = tallyEnt.Value;
                }
            }
            return result;
        }
    }
}
