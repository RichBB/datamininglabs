﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnsembleMethods 
{
    /// <summary>
    /// Represents an instance of the decision tree base learner.
    /// Each instance of DecisionTreeModule encapstulate a trained
    /// tree and exposes interfaces to classify a new test entry.
    /// </summary>
    class DecisionTreeModule
    {
        private static string UNKNOWN_VALUE = "?";
        private double threshold = 1.0;
        private int maxTreeDepth = int.MaxValue;
        private DecisionTreeNode root = new DecisionTreeNode();
        private List<Attribute> attributes = new List<Attribute>();
        private List<List<string>> examples = new List<List<string>>();
        private Dictionary<string, List<string>> mostCommonAttrValsForClass = new Dictionary<string, List<string>>();

        public DecisionTreeModule(List<List<string>> exs, List<Attribute> attrs, double th, int maxDepth = int.MaxValue)
        {
            this.examples = exs;
            this.attributes = attrs;
            this.threshold = th;
            if (maxDepth >= 0)
            {
                this.maxTreeDepth = maxDepth;
            }
            int classAttrIdx = (attributes.Count - 1);
            HashSet<int> attrToTestIdxs = GetAttrToTestIdxs(classAttrIdx);
            ID3(root, examples, classAttrIdx, attrToTestIdxs, 0);
            mostCommonAttrValsForClass = ExtractMostCommonAttrValsForClass(classAttrIdx, examples);
        }

        private HashSet<int> GetAttrToTestIdxs(int classAttrIdx)
        {
            // In the given dataset, the class attr happens to be the attribute on the last index.
            HashSet<int> attrToTestIdxs = new HashSet<int>();
            for (int i = 0; i < attributes.Count - 1; i++)
            {
                // Exclude the class Attribute becuase it doesn't make sense to split the tree by it
                if (i != classAttrIdx)
                {
                    attrToTestIdxs.Add(i);
                }
            }
            return attrToTestIdxs;
        }

        /// <summary>
        /// Helper function to tally attribute values for a particular attribute in the given set of exmaples.
        /// </summary>
        /// <param name="examples"></param>
        /// <param name="targetAttrIdx"></param>
        /// <returns></returns>
        private Dictionary<string, int> TallyAttr(List<List<string>> examples, int targetAttrIdx)
        {
            Dictionary<string, int> tally = new Dictionary<string, int>();
            foreach (List<string> example in examples)
            {
                string exampleClass = example[targetAttrIdx];
                if (tally.ContainsKey(exampleClass))
                {
                    tally[exampleClass] = tally[exampleClass] + 1;
                }
                else
                {
                    tally[exampleClass] = 1;
                }
            }
            return tally;
        }


        /// <summary>
        /// Return the majority value
        /// Return null if we can't work out a majority from the given exmaples.
        /// </summary>
        /// <param name="examples"></param>
        /// <param name="classAttrIdx"></param>
        /// <returns></returns>
        private string GetMajorityValue(List<List<string>> examples, int classAttrIdx)
        {
            string majorityValue = null;
            if (examples != null)
            {
                Dictionary<string, int> tally = TallyAttr(examples, classAttrIdx);
                int maxTally = -1;
                foreach (KeyValuePair<string, int> entry in tally)
                {
                    if (entry.Value > maxTally)
                    {
                        maxTally = entry.Value;
                        majorityValue = entry.Key;
                    }
                }
            }
            return majorityValue;
        }

        private string GetConvergedValue(List<List<string>> examples, int classAttrIdx)
        {
            string convergedVal = null;
            if (examples != null && examples.Count > 0)
            {
                string potentialConvergedClass = examples[0][classAttrIdx];
                bool examplesConverged = true;
                for (int i = 1; i < examples.Count; i++)
                {
                    string exampleClass = examples[i][classAttrIdx];
                    if (exampleClass != potentialConvergedClass)
                    {
                        examplesConverged = false;
                        break;
                    }
                }
                if (examplesConverged)
                {
                    convergedVal = potentialConvergedClass;
                }
            }
            return convergedVal;
        }

        private double CalculateEntropy(List<List<string>> examples, int classAttrIdx)
        {
            double entropy = 0.0;
            Dictionary<string, int> tally = TallyAttr(examples, classAttrIdx);

            foreach (KeyValuePair<string, int> entry in tally)
            {
                double pv = (double) entry.Value / examples.Count;
                entropy += (-1) * pv * Math.Log(pv, 2.0);
            }
            return entropy;
        }

        private SplittingAttributeInfo GetAttributeWithHighestInfoGain(List<List<string>> examples, int classAttrIdx, HashSet<int> attrToTestIdxs)
        {
            double entrophyS = CalculateEntropy(examples, classAttrIdx);
            double maxInformationGain = double.MinValue;
            SplittingAttributeInfo bestAttr = new SplittingAttributeInfo();
            bestAttr.AttrIdx = 0;
            bestAttr.AggrExamplesByAttrVal = new Dictionary<string, List<List<string>>>();
            foreach (int attrIdx in attrToTestIdxs)
            {
                Dictionary<string, List<List<string>>> aggrExamplesByAttrVal = new Dictionary<string, List<List<string>>>();
                foreach (List<string> example in examples)
                {
                    string attrVal = example[attrIdx];
                    if (!aggrExamplesByAttrVal.ContainsKey(attrVal))
                    {
                        aggrExamplesByAttrVal[attrVal] = new List<List<string>>();
                    }
                    aggrExamplesByAttrVal[attrVal].Add(example);
                }

                double entropyForAttrIdx = 0.0;
                foreach (KeyValuePair<string, List<List<string>>> aggrResult in aggrExamplesByAttrVal)
                {
                    double expectedValue = (double) aggrResult.Value.Count / examples.Count;
                    entropyForAttrIdx += expectedValue * CalculateEntropy(aggrResult.Value, classAttrIdx);
                }
                double infoGainForAttrIdx = entrophyS - entropyForAttrIdx;
                if (infoGainForAttrIdx > maxInformationGain)
                {
                    maxInformationGain = infoGainForAttrIdx;
                    bestAttr.AttrIdx = attrIdx;
                    bestAttr.AggrExamplesByAttrVal = aggrExamplesByAttrVal;
                }
            }
            return bestAttr;
        }

        private double ChiSquareForAttribute(List<List<string>> examples, int classAttrIdx, SplittingAttributeInfo bestAttrCandidate)
        {
            double chiSquareVal = 0.0;
            Dictionary<string, int> tally = TallyAttr(examples, classAttrIdx);
            Dictionary<string, double> expectedRatioForClass = new Dictionary<string, double>();
            foreach (var classTally in tally)
            {
                double expectedRatio = (double)classTally.Value / examples.Count;
                expectedRatioForClass.Add(classTally.Key, expectedRatio);
            }

            foreach (var attrToExamples in bestAttrCandidate.AggrExamplesByAttrVal)
            {
                Dictionary<string, int> attrValTally = TallyAttr(attrToExamples.Value, classAttrIdx);
                foreach (var possibleAttrVal in attributes[classAttrIdx].AttributeVals)
                {
                    if (!attrValTally.ContainsKey(possibleAttrVal))
                    {
                        attrValTally.Add(possibleAttrVal, 0);
                    }
                }
                foreach (var attrClassTally in attrValTally)
                {
                    double xPrime = attrToExamples.Value.Count * expectedRatioForClass[attrClassTally.Key];
                    chiSquareVal += Math.Pow(((double)attrClassTally.Value - xPrime), 2) / xPrime;
                }
            }
            return chiSquareVal;
        }

        private void ID3(DecisionTreeNode root, List<List<string>> examples, int targetAttrIdx, HashSet<int> attrToTestIdxs, int curDepth)
        {
            // Base Cases:
            // If all examples are positive, Return the single-node tree Root, with label = +
            // If all examples are negative, Return the single-node tree Root, with label = -
            // If Attributes (attrToTestIdxs) is empty, Return the single-node tree Root, with label = most common value of Target_attribute
            if (examples != null && examples.Count > 0)
            {
                string convergedLabel = GetConvergedValue(examples, targetAttrIdx);
                if (convergedLabel != null)
                {
                    root.Label = convergedLabel;
                    return;
                }
            }
            if (attrToTestIdxs == null || attrToTestIdxs.Count <= 0)
            {
                string majorityLabel = GetMajorityValue(examples, targetAttrIdx);
                if (majorityLabel != null)
                {
                    root.Label = majorityLabel;
                }
                return;
            }

            // Recursive Case
            SplittingAttributeInfo bestAttr = GetAttributeWithHighestInfoGain(examples, targetAttrIdx, attrToTestIdxs);

            // We will stop splitting the tree if either of the following conditions holds true:
            // 1. If by ChiSquar test, the information gain doesn't justify splitting anymore
            // 2. The maxTreeDepth limit has been hit. This is a user-defined parameter to control the depth of the tree.
            double chiSquareVal = ChiSquareForAttribute(examples, targetAttrIdx, bestAttr);
            double critChiVal = Chi.critchi(threshold, bestAttr.AggrExamplesByAttrVal.Count);
            if ((chiSquareVal < critChiVal) || 
                (curDepth >= maxTreeDepth))
            {
                // One of the conditions to stop splitting further is met, pick up the majority and make a leaf node.
                string majorityLabel = GetMajorityValue(examples, targetAttrIdx);
                if (majorityLabel != null)
                {
                    root.Label = majorityLabel;
                }
                return;
            }

            // If we reach this point, it means the attribute is worth splitting on
            root.SplittingAttributeName = attributes[bestAttr.AttrIdx].AttributeName;
            root.SplittingAttributeIdx = bestAttr.AttrIdx;
            attrToTestIdxs.Remove(bestAttr.AttrIdx);

            string backupLabel = null;
            foreach (string possibleAttrVal in attributes[bestAttr.AttrIdx].AttributeVals)
            {
                // If ExamplesVi is empty, create a leaf node with label=most common TargetAttr in Examples
                if (!bestAttr.AggrExamplesByAttrVal.ContainsKey(possibleAttrVal))
                {
                    if (backupLabel == null)
                    {
                        backupLabel = GetMajorityValue(examples, targetAttrIdx);
                    }
                    DecisionTreeNode newLeaf = new DecisionTreeNode() { Label = backupLabel };
                    root.Children.Add(possibleAttrVal, newLeaf);
                }
                // Else recurse and add a subtree
                else
                {
                    DecisionTreeNode newNode = new DecisionTreeNode();
                    root.Children.Add(possibleAttrVal, newNode);
                    ID3(newNode, bestAttr.AggrExamplesByAttrVal[possibleAttrVal], targetAttrIdx, attrToTestIdxs, (curDepth + 1));
                }
            }
        }

        private Dictionary<string, List<string>> ExtractMostCommonAttrValsForClass(int trueResultIdx, List<List<string>> instances)
        {
            int decisionAttrCount = attributes.Count - 1;
            var attrTallyByClass = new Dictionary<string, List<Dictionary<string, int>>>(attributes[trueResultIdx].AttributeVals.Count);
            foreach (var classVal in attributes[trueResultIdx].AttributeVals)
            {
                attrTallyByClass.Add(classVal, new List<Dictionary<string, int>>(decisionAttrCount));
                for (int i = 0; i < decisionAttrCount; i++)
                {
                    attrTallyByClass[classVal].Add(new Dictionary<string, int>());
                }
            }
            foreach (var instance in instances)
            {
                List<Dictionary<string, int>> attrTally = attrTallyByClass[instance[trueResultIdx]];
                for (int i = 0; i < decisionAttrCount; i++)
                {
                    if (attrTally[i].ContainsKey(instance[i]))
                    {
                        attrTally[i][instance[i]]++;
                    }
                    else
                    {
                        attrTally[i][instance[i]] = 1;
                    }
                }
            }
            var mostCommonAttrValsForClass = new Dictionary<string, List<string>>(attrTallyByClass.Count);
            foreach (var classTallyData in attrTallyByClass)
            {
                var classVal = classTallyData.Key;
                var attrTally = classTallyData.Value;
                mostCommonAttrValsForClass.Add(classVal, new List<string>(decisionAttrCount));
                for (int i = 0; i < decisionAttrCount; i++)
                {
                    mostCommonAttrValsForClass[classVal].Add("");
                }
                for (int i = 0; i < attrTally.Count; i++)
                {
                    Dictionary<string, int> tallyData = attrTally[i];
                    string mostCommonVal = "";
                    int mostCommonCount = -1;
                    foreach (var tallyEntry in tallyData)
                    {
                        if (tallyEntry.Value > mostCommonCount)
                        {
                            mostCommonVal = tallyEntry.Key;
                            mostCommonCount = tallyEntry.Value;
                        }
                    }
                    mostCommonAttrValsForClass[classVal][i] = mostCommonVal;
                }
            }
            return mostCommonAttrValsForClass;
        }

        public string PredictOutcome(List<string> newInstance, out string predictionTrace)
        {
            int classAttrIdx = (attributes.Count - 1);
            DecisionTreeNode curNode = root;
            StringBuilder sb = new StringBuilder();
            sb.Append(curNode.SplittingAttributeName).Append("->");
            while (string.IsNullOrEmpty(curNode.Label))
            {
                string splittingAttr = newInstance[curNode.SplittingAttributeIdx];
                if (splittingAttr == UNKNOWN_VALUE)
                {
                    // In the case of unkonwn value, first consult the most common attribute value.
                    string mostCommonAttrVal = mostCommonAttrValsForClass[newInstance[classAttrIdx]][curNode.SplittingAttributeIdx];
                    if (mostCommonAttrVal == UNKNOWN_VALUE)
                    {
                        // Too many unkonwns to guess confidently.
                        sb.Append("Too Many Unkonwn -> 0");
                        predictionTrace = sb.ToString();
                        return "0";
                    }
                    else
                    {
                        // Continue traversing the tree with the most common value of the same target
                        curNode = curNode.Children[mostCommonAttrVal];
                        sb.Append(curNode.SplittingAttributeName).Append("->");
                    }
                }
                else
                {
                    if (curNode.Children.ContainsKey(splittingAttr))
                    {
                        curNode = curNode.Children[splittingAttr];
                        sb.Append(curNode.SplittingAttributeName).Append("->");
                    }
                    else
                    {
                        string guess = "0";
                        sb.Append("NDGuess:" + guess);
                        predictionTrace = sb.ToString();
                        return guess;
                    }
                }
            }
            sb.Append(curNode.Label);
            predictionTrace = sb.ToString();
            return curNode.Label;
        }
        public void primitivePrintTree()
        {
            Queue<DecisionTreeNode> q = new Queue<DecisionTreeNode>();
            q.Enqueue(root);
            int curLvlCount = 1;
            int nextLvlCount = 0;

            while(q.Count > 0)
            {
                DecisionTreeNode n = q.Dequeue();
                curLvlCount--;
                foreach (KeyValuePair<string, DecisionTreeNode> c in n.Children)
                {
                    q.Enqueue(c.Value);
                }
                nextLvlCount += n.Children.Count;
                Console.Write(n);

                if (curLvlCount == 0)
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    curLvlCount = nextLvlCount;
                    nextLvlCount = 0;
                }
            }
        }
    }
}
