﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnsembleMethods
{
    public class DecisionTreeNode 
    {
        public Dictionary<string, DecisionTreeNode> Children { get; set; } = new Dictionary<string, DecisionTreeNode>();
        public string SplittingAttributeName { get; set; } = "";
        public int SplittingAttributeIdx { get; set; } = -1;

        // For leaf node
        public string Label { get; set; } = null;

        public override string ToString()
        {
            return string.Format(" [Split[{0}]: {1}, Lbl: {2}, CC: {3}] ", SplittingAttributeIdx, SplittingAttributeName, Label, Children.Count);
        }
    }
}
