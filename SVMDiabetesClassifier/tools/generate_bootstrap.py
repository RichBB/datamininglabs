import sys
import random

if __name__ == '__main__':
    examples = []
    with open('diabetes_libsvmformat_train.txt') as train_file:
        for line in train_file:
            examples.append(line)

    num_examples = len(examples)
    for i in range(10):
        new_sample_file = 'diabetes_libsvmformat_train' + str(i) + '.txt'
        with open(new_sample_file, 'w+') as f:
            for c in range(num_examples):
                f.write(examples[random.randint(0, num_examples - 1)])
        print('Bootstrapped {0} examples into {1}'.format(num_examples, new_sample_file))
