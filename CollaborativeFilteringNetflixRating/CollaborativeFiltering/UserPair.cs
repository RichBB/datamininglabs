﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollaborativeFiltering
{
    /// <summary>
    /// UserPair contains 2 user ids. Two UserPair objects are consider equal
    /// as long as the 2 user ids they contain are the same. 
    /// (e.g. even when id1 and id2 are swapped, two objects should still be considered equal)
    /// The motivation behind this equality concept is so that we can use UserPair as the "Key" to other
    /// data structures such as the Dictionary, to track pari-wise Pearson Coeeficient.
    /// </summary>
    public class UserPair
    {
        public int Id1 { get; set; }
        public int Id2 { get; set; }

        /// <summary>
        /// UserPair contains 2 user ids. Two UserPair object are considered equal
        /// as long as the 2 user ids they contain are the same. 
        /// (e.g. even when id1 and id2 are swapped, two objects should still be considered equal)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>true if the 2 internal ids are the same</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var other = (UserPair)obj;
            return (Id1 == other.Id1 && Id2 == other.Id2) ||
                   (Id1 == other.Id2 && Id2 == other.Id1);
        }

        /// <summary>
        /// Defined as the sum of 2 internal IDs' hashcode.
        /// So long as Equals() function return true, the 2
        /// objects' retuning hashcode should be the same.
        /// </summary>
        /// <returns>Sum of the hashcode of the 2 ids</returns>
        public override int GetHashCode()
        {
            return Id1.GetHashCode() + Id2.GetHashCode();
        }
    }
}
