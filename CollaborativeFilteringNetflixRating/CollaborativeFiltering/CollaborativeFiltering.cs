﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CollaborativeFiltering
{
    class CollaborativeFiltering
    {
        // == Tweakable Parameters ==
        public static int MAX_NUM_RATERS_TO_CONSIDER = 1500;
        public static int NUM_NEIGHBORS_TO_CONSIDER = 30;
        public static double MIN_SIMILARITY_REQUIREMENT = 2.5;

        private static string MOVIE_TITLE_PATH = @"..\..\Data\movie_titles.txt";
        private static string TRAINING_RATINGS_PATH = @"..\..\Data\TrainingRatings.txt";
        private static string TESTING_RATINGS_PATH = @"..\..\Data\TestingRatings.txt";
        private static int MY_USER_ID = 0;
        private static int CACHE_SIZE = 3000000;

        // ratingData stores the information read from TrainingRating.txt into the following data structure:
        // {UserId -> {MovieId -> Rating}}
        private Dictionary<int, Dictionary<int, double>> ratingData = new Dictionary<int, Dictionary<int, double>>(28979);

        // movieRaters cache the set of people who has rated a particular movie
        // {MovieId -> Set{UserIds}}
        private Dictionary<int, ISet<int>> movieRaters = new Dictionary<int, ISet<int>>(1821);

        // movies stores the information read from movie_title.txt:
        // {MovieId -> MovieTitle}
        private Dictionary<int, MovieTitle> movies = new Dictionary<int, MovieTitle>(17770);

        // Intermediate computed result of every user's average rating. This helps with performance. 
        // {UserId -> Average Rating}
        private Dictionary<int, double> userAvgRating = new Dictionary<int, double>(28979);

        // Cache the pari-wise Similarity (Pearson Coefficient) for a pair of users.
        // {UserPair -> double}
        private Dictionary<UserPair, double> similarityCache = new Dictionary<UserPair, double>(CACHE_SIZE);

        private void ReadRatingData(string path)
        {
            using (StreamReader reader = File.OpenText(path))
            {
                string line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    string[] entry = line.Split(',');
                    if (entry.Length == 3)
                    {
                        int movieId = int.Parse(entry[0]);
                        int userId = int.Parse(entry[1]);
                        double rating = double.Parse(entry[2]);

                        // Read into ratingData
                        if (!ratingData.ContainsKey(userId))
                        {
                            ratingData.Add(userId, new Dictionary<int, double>());
                        }
                        Dictionary<int, double> userRatings = ratingData[userId];
                        if (!userRatings.ContainsKey(movieId))
                        {
                            userRatings.Add(movieId, rating);
                        }

                        // Read into movieRaters
                        if (!movieRaters.ContainsKey(movieId))
                        {
                            movieRaters.Add(movieId, new HashSet<int>());
                        }
                        movieRaters[movieId].Add(userId);
                    }
                }
            }
        }

        private void ReadMovieData()
        {
            using (StreamReader reader = File.OpenText(MOVIE_TITLE_PATH))
            {
                string line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    string[] movieData = line.Split(',');
                    if (movieData.Length >= 3)
                    {
                        int movieId = int.Parse(movieData[0]);
                        int year = 0;
                        int.TryParse(movieData[1], out year);
                        string movieName = movieData[2];
                        for (int i = 3; i < movieData.Length; i++)
                        {
                            movieName += movieData[i];
                        }
                        if (!movies.ContainsKey(movieId))
                        {
                            MovieTitle newTitle = new MovieTitle() { ID = movieId, Year = year, MovieName = movieName };
                            movies.Add(movieId, newTitle);
                        }
                    }
                }
            }
        }

        private void CalculateAllUserAvgRating()
        {
            foreach (var userRatingData in ratingData)
            {
                int userId = userRatingData.Key;
                double avgRating = 0.0;
                foreach (var ratingEntry in userRatingData.Value)
                {
                    avgRating += ratingEntry.Value;
                }
                avgRating = avgRating / userRatingData.Value.Count;
                userAvgRating.Add(userId, avgRating);
            }
        }

        private ISet<int> GetMoviesRatedByBothUsers(int activeUserId, int neighborId)
        {
            if (!ratingData.ContainsKey(activeUserId) || !ratingData.ContainsKey(neighborId))
            {
                return new HashSet<int>();
            }
            var moviesRatedByBoth = new HashSet<int>(ratingData[activeUserId].Keys);
            var moviesRatedByNeighbor = new HashSet<int>(ratingData[neighborId].Keys);
            moviesRatedByBoth.IntersectWith(moviesRatedByNeighbor);
            return moviesRatedByBoth;
        }

        /// <summary>
        /// Similarity between two users are defined by Pearson Coefficient of the
        /// movies that are rated by both users.
        /// </summary>
        /// <param name="activeUserId"></param>
        /// <param name="neighborId"></param>
        /// <returns>similarity rating of the two users</returns>
        private double CalculateSimilarity(int activeUserId, int neighborId)
        {
            // Given the same training data, the similarity rating between two users will always be the same.
            // Therefore, at least for the current setup, the similarity rating is cacheable. Try to get the
            // similarity rating from cache first to save compute cycle.
            UserPair userPair = new UserPair() { Id1 = activeUserId, Id2 = neighborId };
            if (similarityCache.ContainsKey(userPair))
            {
                return similarityCache[userPair];
            }

            double numerator = 0.0;
            double denominator = 0.0;
            ISet<int> moviesRatedByBoth = GetMoviesRatedByBothUsers(activeUserId, neighborId);
            foreach (int movieId in moviesRatedByBoth)
            {
                // (Rik - Ri_avg) * (Rjk - Rj_avg)
                double newTerm = (ratingData[activeUserId][movieId] - userAvgRating[activeUserId]) * (ratingData[neighborId][movieId] - userAvgRating[neighborId]);
                numerator += newTerm;
                denominator += (newTerm * newTerm);
            }
            denominator = Math.Sqrt(denominator);

            double similarityRating = 0.0;
            if (denominator != 0.0)
            {
                similarityRating = numerator / denominator;
            }
            // Don't just throw away the hard-earned similarity rating. Cache it first.
            // But clear out the cache if it hits a certain size, considering if we cache everything
            // it can grow quite large. Likely going to run out of memory.
            if (similarityCache.Count >= CACHE_SIZE)
            {
                similarityCache.Clear();
            }
            similarityCache.Add(userPair, similarityRating);
            return similarityRating;
        }

        private ISet<int> GetNearestNeighborsForMovie(int activeUserId, int movieId)
        {
            var result = new HashSet<int>();
            // Only pick from other uesrs who have rated the targeted movie we want to predict for the active user.
            if (movieRaters.ContainsKey(movieId))
            {
                var raters = movieRaters[movieId];
                int cutoff = Math.Min(MAX_NUM_RATERS_TO_CONSIDER, raters.Count);
                for (int i = 0; i < cutoff; i++)
                {
                    int candidateId = raters.ElementAt(i);
                    double similarityRating = CalculateSimilarity(activeUserId, candidateId);
                    if (similarityRating >= MIN_SIMILARITY_REQUIREMENT)
                    {
                        result.Add(candidateId);
                    }
                    if (result.Count >= NUM_NEIGHBORS_TO_CONSIDER)
                    {
                        break;
                    }
                }
            }
            return result;
        }

        private double PredictUserRating(int activeUserId, int movieId)
        {
            double predictedRating = 2.5;
            if (userAvgRating.ContainsKey(activeUserId))
            {
                predictedRating = userAvgRating[activeUserId];
            }

            ISet<int> nearestNeighbors = GetNearestNeighborsForMovie(activeUserId, movieId);
            double ratingAdjustment = 0.0;
            double alphaInverse = 0.0;
            foreach (var neighborId in nearestNeighbors)
            {
                double weight = CalculateSimilarity(activeUserId, neighborId);
                ratingAdjustment += weight * (ratingData[neighborId][movieId] - userAvgRating[neighborId]);
                alphaInverse += Math.Abs(weight);
            }
            if (alphaInverse != 0.0)
            {
                predictedRating += (ratingAdjustment / alphaInverse);
            }
            return predictedRating;
        }

        private void CollaborativeFilteringCore()
        {
            using (StreamReader reader = File.OpenText(TESTING_RATINGS_PATH))
            {
                string line = "";
                int entryCount = 0;
                double meanAbsoluteError = 0.0;
                double rootMeanSquareError = 0.0;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] entry = line.Split(',');
                    if (entry.Length == 3)
                    {
                        entryCount++;
                        int movieToPridictId = int.Parse(entry[0]);
                        int activeUserId = int.Parse(entry[1]);
                        double actualRating = double.Parse(entry[2]);
                        double predictedRating = PredictUserRating(activeUserId, movieToPridictId);
                        if (predictedRating > 5.0) predictedRating = 5.0;
                        if (predictedRating < 1.0) predictedRating = 1.0;
                        Console.WriteLine("User: {0}, Movie: {1} => Predicted: {2}, Actual: {3}", activeUserId, movieToPridictId, predictedRating, actualRating);
                        double diff = Math.Abs(predictedRating - actualRating);
                        meanAbsoluteError += diff;
                        rootMeanSquareError += diff * diff;
                    }
                }
                if (entryCount != 0)
                {
                    meanAbsoluteError = meanAbsoluteError / entryCount;
                    rootMeanSquareError = Math.Sqrt(rootMeanSquareError / entryCount);

                }
                Console.WriteLine("Mean Absolute Error: {0}", meanAbsoluteError);
                Console.WriteLine("Root Mean Square Error: {0}", rootMeanSquareError);
            }
        }

        private void PrintAllMoviesWithRaters()
        {
            foreach (int movieId in movieRaters.Keys)
            {
                Console.WriteLine(movies[movieId]);
            }
        }

        /// <summary>
        /// Extra Credit:
        /// Predict movies for myself.
        /// The function looks at all movies with raters, predict the rating of the movie
        /// and then sort by the predicted values. (high to low)
        /// </summary>
        private void PredictMoviesForMe()
        {
            var predictions = new List<MovieRatingPrediction>(movieRaters.Count);
            foreach (var movieId in movieRaters.Keys)
            {
                if (ratingData[MY_USER_ID].ContainsKey(movieId))
                {
                    // Skip movies I've alraedy rated.
                    continue;
                }
                double predictedRating = PredictUserRating(MY_USER_ID, movieId);
                if (predictedRating > 5.0) predictedRating = 5.0;
                if (predictedRating < 1.0) predictedRating = 1.0;
                var newPrediction = new MovieRatingPrediction() { MovieId = movieId, PredictedRating = predictedRating };
                predictions.Add(newPrediction);
            }
            predictions.Sort(new HighPredictionRatingFirstComparer());
            foreach (var prediction in predictions)
            {
                Console.WriteLine("{0} => {1}", movies[prediction.MovieId], prediction.PredictedRating);
            }
        }

        public void Start()
        {
            Console.WriteLine("Reading Rating Data...");
            ReadRatingData(TRAINING_RATINGS_PATH);
            Console.WriteLine("Reading Movie Data...");
            ReadMovieData();
            Console.WriteLine("Calculating Avg Rating for all users...");
            CalculateAllUserAvgRating();

            Console.WriteLine("Running Collaborative Filtering");
            CollaborativeFilteringCore();

            //PrintAllMoviesWithRaters();
            //Console.WriteLine("Finding Good Movies for Me...");
            //PredictMoviesForMe();
        }

        static void Main(string[] args)
        {
            try
            {
                CollaborativeFiltering main = new CollaborativeFiltering();
                main.Start();
                Console.WriteLine(DateTime.Now);
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("Make sure the Input Data is at the correct location in relation to the executable.");
                Console.WriteLine("See documentation for details.");
                Console.WriteLine(e.Message);
            }
            catch (OutOfMemoryException e)
            {
                Console.WriteLine("By design, this program uses somewhat large amount of RAM for performance reasons");
                Console.WriteLine("Make sure you have at least couple of GB of RAM available when running");
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Press Enter to exit program...");
            Console.ReadLine();
        }
    }
}
