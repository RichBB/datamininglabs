﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollaborativeFiltering
{
    public class SimilarityRatingEntry
    {
        public double SimilarityRating { get; set; }
        public int NeighborId { get; set; }
    }


    public class SimilarFirstComparer : IComparer<SimilarityRatingEntry>
    {
        public int Compare(SimilarityRatingEntry lhs, SimilarityRatingEntry rhs)
        {
            if (lhs.SimilarityRating == rhs.SimilarityRating)
            {
                return 0;
            }
            else if (lhs.SimilarityRating < rhs.SimilarityRating)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }
    }
}
