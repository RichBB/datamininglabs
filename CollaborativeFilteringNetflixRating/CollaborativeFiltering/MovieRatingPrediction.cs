﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollaborativeFiltering
{
    public class MovieRatingPrediction
    {
        public int MovieId { get; set; }
        public double PredictedRating { get; set; }
    }

    public class HighPredictionRatingFirstComparer : IComparer<MovieRatingPrediction>
    {
        public int Compare(MovieRatingPrediction lhs, MovieRatingPrediction rhs)
        {
            if (lhs.PredictedRating == rhs.PredictedRating)
            {
                return 0;
            }
            else if (lhs.PredictedRating < rhs.PredictedRating)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}
