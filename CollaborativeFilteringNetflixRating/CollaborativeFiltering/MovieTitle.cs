﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollaborativeFiltering
{
    public class MovieTitle
    {
        public int ID { get; set; }
        public int Year { get; set; }
        public string MovieName { get; set; }

        public override string ToString()
        {
            return ID + "," + Year + "," + MovieName;
        }
    }
}
