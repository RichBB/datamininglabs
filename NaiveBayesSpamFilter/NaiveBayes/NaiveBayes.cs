﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaiveBayes
{
    class NaiveBayes
    {
        public static string TRAINING_DATA_PATH = @"../../Data/train";
        public static string TESTING_DATA_PATH = @"../../Data/test";

        private static int CLASS_IDX = 1;
        private static string SPAM = "spam";

        private double emailEntryCount = 0;
        private double hamCount = 0;
        private double spamCount = 0;
        private double pHam = 0.0;
        private double pSpam = 0.0;
        private Dictionary<string, int> allVocabulary = new Dictionary<string, int>();
        private Dictionary<string, int> hamVocabulary = new Dictionary<string, int>();
        private Dictionary<string, int> spamVocabulary = new Dictionary<string, int>();
        private Dictionary<string, double> hamVocabularyProb = new Dictionary<string, double>();
        private Dictionary<string, double> spamVocabularyProb = new Dictionary<string, double>();

        private void AddToVocabHelper(IDictionary<string, int> targetDic, string word, int occuranceCount)
        {
            if (!targetDic.ContainsKey(word))
            {
                targetDic.Add(word, occuranceCount);
            }
            else
            {
                targetDic[word] = targetDic[word] + occuranceCount;
            }
        }

        private void ReadEmailVocabCounts(string[] emailData, bool isSpam)
        {
            bool readWord = false;
            string curWord = null;
            for (int i = 2; i < emailData.Length; i++)
            {
                int occuranceCount = 0;
                bool isNum = int.TryParse(emailData[i], out occuranceCount);
                if (!isNum)
                {
                    curWord = emailData[i];
                    readWord = true;
                }
                else if (isNum && readWord && curWord != null)
                {
                    AddToVocabHelper(allVocabulary, curWord, occuranceCount);
                    if (isSpam)
                    {
                        AddToVocabHelper(spamVocabulary, curWord, occuranceCount);
                    }
                    else
                    {
                        AddToVocabHelper(hamVocabulary, curWord, occuranceCount);
                    }
                    curWord = null;
                    readWord = false;
                }
            }
        }

        private void ReadTrainingData()
        {
            using (StreamReader sr = File.OpenText(TRAINING_DATA_PATH))
            {
                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    emailEntryCount++;
                    string[] emailData = line.Split(' ');
                    bool isSpam = SPAM.Equals(emailData[CLASS_IDX], StringComparison.CurrentCultureIgnoreCase);
                    if (isSpam)
                    {
                        spamCount++;
                    }
                    else
                    {
                        hamCount++;
                    }
                    ReadEmailVocabCounts(emailData, isSpam);
                }
            }
        }

        private void CalculateSpamHamProb()
        {
            if (emailEntryCount > 0)
            {
                pHam = hamCount / emailEntryCount;
                pSpam = spamCount / emailEntryCount;
            }
        }

        private void CalculateWordConditionProbsHelper(IDictionary<string, int> sourceVocabs, IDictionary<string, double> targetStorage)
        {
            double n = 0;
            foreach (var vocabData in sourceVocabs)
            {
                n += vocabData.Value;
            }
            foreach (var vocabData in allVocabulary)
            {
                double denominator = (n + allVocabulary.Count);
                double numerator = 0;
                if (sourceVocabs.ContainsKey(vocabData.Key))
                {
                    numerator += sourceVocabs[vocabData.Key];
                }
                double wordProb = numerator / denominator;
                // Since that each conditional probability tend to be small,
                // use the Log of the probability to reduce the chance of having
                // overflow/underflow. Note that this means later when we're supposed to
                // multiply conditional prob of words, we will be using additions instead.
                wordProb = Math.Log10(wordProb);
                targetStorage.Add(vocabData.Key, wordProb);
            }
        }

        private void CalculateWordConditionProbs()
        {
            CalculateWordConditionProbsHelper(spamVocabulary, spamVocabularyProb);
            CalculateWordConditionProbsHelper(hamVocabulary, hamVocabularyProb);
        }

        private void NaiveBayesCore()
        {
            double totalCount = 0;
            double correctPredictionCount = 0;
            using (StreamReader sr = File.OpenText(TESTING_DATA_PATH))
            {
                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    totalCount++;
                    string[] emailData = line.Split(' ');
                    bool isSpam = SPAM.Equals(emailData[CLASS_IDX], StringComparison.CurrentCultureIgnoreCase);
                    double VnbSpam = pSpam;
                    double VnbHam = pHam;
                    for (int i = 2; i < emailData.Length; i++)
                    {
                        int testParseNum = 0;
                        bool isNum = int.TryParse(emailData[i], out testParseNum);
                        if (!isNum)
                        {
                            // Since the probability are stored as the log10 of the real value, we use addition here
                            if (spamVocabularyProb.ContainsKey(emailData[i]))
                            {
                                VnbSpam += spamVocabularyProb[emailData[i]];
                            }
                            if (hamVocabularyProb.ContainsKey(emailData[i]))
                            {
                                VnbHam += hamVocabularyProb[emailData[i]];
                            }
                        }
                    }
                    bool predictIsSpam = (VnbSpam > VnbHam);
                    if (isSpam == predictIsSpam)
                    {
                        correctPredictionCount++;
                    }
                }
            }
            Console.WriteLine("# Test Entries: {0}, # Correct Predictions: {1}, Accuracy Rate: {2}", totalCount, correctPredictionCount, correctPredictionCount/totalCount);
        }

        public void Start()
        {
            Console.WriteLine("Learning Naive Bayes Text...");
            ReadTrainingData();
            CalculateSpamHamProb();
            CalculateWordConditionProbs();
            Console.WriteLine("Classify Naive Bayes Text...");
            NaiveBayesCore();
        }

        static void Main(string[] args)
        {
            try
            {
                NaiveBayes main = new NaiveBayes();
                main.Start();
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("Make sure the training and testing data are in the correct directory in relation to the executable.");
                Console.WriteLine("See documentation for more details.");
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
