﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;

namespace ClickstreamMining
{
    class ClickstreamMining
    {
        //private static string TRAINING_DATA_RELATIVE_PATH = @"..\..\Data\test_program_subsetD.arff";
        private static string TRAINING_DATA_RELATIVE_PATH = @"..\..\Data\training_subsetD.arff";
        private static string TEST_DATA_RELATIVE_PATH = @"..\..\Data\testingD.arff";
        private static string ATTRIBUTE_ANNOTATION = "@attribute";
        private static string DATA_ANNOTATION = "@data";
        private static string UNKNOWN_VALUE = "?";
        private static double THRESHOLD = 1;
        private List<Attribute> attributes = new List<Attribute>(275);
        private List<List<string>> examples = new List<List<string>>(50000);
        private DecisionTreeNode root = new DecisionTreeNode();

        private void ExtractAttribute(string s)
        {
            string attrData = s.Substring(ATTRIBUTE_ANNOTATION.Length);
            int valStartIdx = attrData.IndexOf('{');
            string attrName = attrData.Substring(0, valStartIdx).Trim();
            string attrValsStr = attrData.Substring(valStartIdx + 1).Trim('}');
            HashSet<string> attrValsSet = new HashSet<string>();
            foreach(string val in attrValsStr.Split(','))
            {
                attrValsSet.Add(val);
            }
            Attribute newAttribute = new Attribute() { AttributeName = attrName, AttributeVals = attrValsSet };
            attributes.Add(newAttribute);
        }

        private void ReadTrainingData()
        {
            using (StreamReader sr = File.OpenText(TRAINING_DATA_RELATIVE_PATH))
            {
                string s = "";
                bool inDataSection = false;
                while ((s = sr.ReadLine()) != null)
                {
                    if (inDataSection)
                    {
                        List<string> newExample = new List<string>(s.Split(','));
                        if (newExample.Count == attributes.Count)
                        {
                            examples.Add(newExample);
                        }
                    }
                    else if (s.StartsWith(ATTRIBUTE_ANNOTATION))
                    {
                        ExtractAttribute(s);
                    }
                    else if (s.StartsWith(DATA_ANNOTATION))
                    {
                        inDataSection = true;
                    }
                }
            }
        }

        /// <summary>
        /// Return the converged (class) value if all of the examples are off the same class.
        /// Return null if there wasn't a converged value. (at least one example is different from the rest)
        /// </summary>
        /// <param name="examples"></param>
        /// <param name="classAttrIdx"></param>
        /// <returns></returns>
        private string GetConvergedValue(List<List<string>> examples, int classAttrIdx)
        {
            string convergedVal = null;
            if (examples != null && examples.Count > 0)
            {
                string potentialConvergedClass = examples[0][classAttrIdx];
                bool examplesConverged = true;
                for (int i = 1; i < examples.Count; i++)
                {
                    string exampleClass = examples[i][classAttrIdx];
                    if (exampleClass != potentialConvergedClass)
                    {
                        examplesConverged = false;
                        break;
                    }
                }
                if (examplesConverged)
                {
                    convergedVal = potentialConvergedClass;
                }
            }
            return convergedVal;
        }

        /// <summary>
        /// Helper function to tally attribute values for a particular attribute in the given set of exmaples.
        /// </summary>
        /// <param name="examples"></param>
        /// <param name="targetAttrIdx"></param>
        /// <returns></returns>
        private Dictionary<string, int> TallyAttr(List<List<string>> examples, int targetAttrIdx)
        {
            Dictionary<string, int> tally = new Dictionary<string, int>();
            foreach (List<string> example in examples)
            {
                string exampleClass = example[targetAttrIdx];
                if (tally.ContainsKey(exampleClass))
                {
                    tally[exampleClass] = tally[exampleClass] + 1;
                }
                else
                {
                    tally[exampleClass] = 1;
                }
            }
            return tally;
        }

        /// <summary>
        /// Return the majority value
        /// Return null if we can't work out a majority from the given exmaples.
        /// </summary>
        /// <param name="examples"></param>
        /// <param name="classAttrIdx"></param>
        /// <returns></returns>
        private string GetMajorityValue(List<List<string>> examples, int classAttrIdx)
        {
            string majorityValue = null;
            if (examples != null)
            {
                Dictionary<string, int> tally = TallyAttr(examples, classAttrIdx);
                int maxTally = -1;
                foreach (KeyValuePair<string, int> entry in tally)
                {
                    if (entry.Value > maxTally)
                    {
                        maxTally = entry.Value;
                        majorityValue = entry.Key;
                    }
                }
            }
            return majorityValue;
        }

        private double CalculateEntropy(List<List<string>> examples, int classAttrIdx)
        {
            double entropy = 0.0;
            Dictionary<string, int> tally = TallyAttr(examples, classAttrIdx);

            foreach (KeyValuePair<string, int> entry in tally)
            {
                double pv = (double) entry.Value / examples.Count;
                entropy += (-1) * pv * Math.Log(pv, 2.0);
            }
            return entropy;
        }

        private SplittingAttributeInfo GetAttributeWithHighestInfoGain(List<List<string>> examples, int classAttrIdx, HashSet<int> attrToTestIdxs)
        {
            double entrophyS = CalculateEntropy(examples, classAttrIdx);
            double maxInformationGain = double.MinValue;
            SplittingAttributeInfo bestAttr = new SplittingAttributeInfo();
            bestAttr.AttrIdx = 0;
            bestAttr.AggrExamplesByAttrVal = new Dictionary<string, List<List<string>>>();
            foreach (int attrIdx in attrToTestIdxs)
            {
                Dictionary<string, List<List<string>>> aggrExamplesByAttrVal = new Dictionary<string, List<List<string>>>();
                foreach (List<string> example in examples)
                {
                    string attrVal = example[attrIdx];
                    if (!aggrExamplesByAttrVal.ContainsKey(attrVal))
                    {
                        aggrExamplesByAttrVal[attrVal] = new List<List<string>>();
                    }
                    aggrExamplesByAttrVal[attrVal].Add(example);
                }

                double entropyForAttrIdx = 0.0;
                foreach (KeyValuePair<string, List<List<string>>> aggrResult in aggrExamplesByAttrVal)
                {
                    double expectedValue = (double) aggrResult.Value.Count / examples.Count;
                    entropyForAttrIdx += expectedValue * CalculateEntropy(aggrResult.Value, classAttrIdx);
                }
                double infoGainForAttrIdx = entrophyS - entropyForAttrIdx;
                if (infoGainForAttrIdx > maxInformationGain)
                {
                    maxInformationGain = infoGainForAttrIdx;
                    bestAttr.AttrIdx = attrIdx;
                    bestAttr.AggrExamplesByAttrVal = aggrExamplesByAttrVal;
                }
            }
            return bestAttr;
        }

        private double ChiSquareForAttribute(List<List<string>> examples, int classAttrIdx, SplittingAttributeInfo bestAttrCandidate)
        {
            double chiSquareVal = 0.0;
            Dictionary<string, int> tally = TallyAttr(examples, classAttrIdx);
            Dictionary<string, double> expectedRatioForClass = new Dictionary<string, double>();
            foreach (var classTally in tally)
            {
                double expectedRatio = (double)classTally.Value / examples.Count;
                expectedRatioForClass.Add(classTally.Key, expectedRatio);
            }

            foreach (var attrToExamples in bestAttrCandidate.AggrExamplesByAttrVal)
            {
                Dictionary<string, int> attrValTally = TallyAttr(attrToExamples.Value, classAttrIdx);
                foreach (var possibleAttrVal in attributes[classAttrIdx].AttributeVals)
                {
                    if (!attrValTally.ContainsKey(possibleAttrVal))
                    {
                        attrValTally.Add(possibleAttrVal, 0);
                    }
                }
                foreach (var attrClassTally in attrValTally)
                {
                    double xPrime = attrToExamples.Value.Count * expectedRatioForClass[attrClassTally.Key];
                    chiSquareVal += Math.Pow(((double)attrClassTally.Value - xPrime), 2) / xPrime;
                }
            }
            return chiSquareVal;
        }

        private void ID3(DecisionTreeNode root, List<List<string>> examples, int targetAttrIdx, HashSet<int> attrToTestIdxs)
        {
            // Base Cases:
            // If all examples are positive, Return the single-node tree Root, with label = +
            // If all examples are negative, Return the single-node tree Root, with label = -
            // If Attributes (attrToTestIdxs) is empty, Return the single-node tree Root, with label = most common value of Target_attribute
            if (examples != null && examples.Count > 0)
            {
                string convergedLabel = GetConvergedValue(examples, targetAttrIdx);
                if (convergedLabel != null)
                {
                    root.Label = convergedLabel;
                    return;
                }
            }
            if (attrToTestIdxs == null || attrToTestIdxs.Count <= 0)
            {
                string majorityLabel = GetMajorityValue(examples, targetAttrIdx);
                if (majorityLabel != null)
                {
                    root.Label = majorityLabel;
                }
                return;
            }

            // Recursive Case
            SplittingAttributeInfo bestAttr = GetAttributeWithHighestInfoGain(examples, targetAttrIdx, attrToTestIdxs);

            // Determine if it's worth splitting further using ChiSquare test.
            double chiSquareVal = ChiSquareForAttribute(examples, targetAttrIdx, bestAttr);
            double critChiVal = Chi.critchi(THRESHOLD, bestAttr.AggrExamplesByAttrVal.Count);
            if (chiSquareVal < critChiVal)
            {
                // It's not worth splitting further, pick up the majority and make a leaf node.
                string majorityLabel = GetMajorityValue(examples, targetAttrIdx);
                if (majorityLabel != null)
                {
                    root.Label = majorityLabel;
                }
                return;
            }

            // It is worth splitting
            root.SplittingAttributeName = attributes[bestAttr.AttrIdx].AttributeName;
            root.SplittingAttributeIdx = bestAttr.AttrIdx;
            attrToTestIdxs.Remove(bestAttr.AttrIdx);

            string backupLabel = null;
            foreach (string possibleAttrVal in attributes[bestAttr.AttrIdx].AttributeVals)
            {
                // If ExamplesVi is empty, create a leaf node with label=most common TargetAttr in Examples
                if (!bestAttr.AggrExamplesByAttrVal.ContainsKey(possibleAttrVal))
                {
                    if (backupLabel == null)
                    {
                        backupLabel = GetMajorityValue(examples, targetAttrIdx);
                    }
                    DecisionTreeNode newLeaf = new DecisionTreeNode() { Label = backupLabel };
                    root.Children.Add(possibleAttrVal, newLeaf);
                }
                // Else recurse and add a subtree
                else
                {
                    DecisionTreeNode newNode = new DecisionTreeNode();
                    root.Children.Add(possibleAttrVal, newNode);
                    ID3(newNode, bestAttr.AggrExamplesByAttrVal[possibleAttrVal], targetAttrIdx, attrToTestIdxs);
                }
            }
        }

        private string PredictOutcome(List<string> newInstance, int classAttrIdx, Dictionary<string, List<string>> mostCommonAttrValsForClass, out string predictionTrace)
        {
            DecisionTreeNode curNode = root;
            StringBuilder sb = new StringBuilder();
            sb.Append(curNode.SplittingAttributeName).Append("->");
            while (string.IsNullOrEmpty(curNode.Label))
            {
                string splittingAttr = newInstance[curNode.SplittingAttributeIdx];
                if (splittingAttr == UNKNOWN_VALUE)
                {
                    //*/
                    // In the case of unkonwn value, first consult the most common attribute value.
                    string mostCommonAttrVal = mostCommonAttrValsForClass[newInstance[classAttrIdx]][curNode.SplittingAttributeIdx];
                    if (mostCommonAttrVal == UNKNOWN_VALUE)
                    {
                        // Too many unkonwns to guess confidently. Just use randomness as the last resort.
                        var guessSpace = attributes[classAttrIdx].AttributeVals;
                        var randomGuess = guessSpace.ToList()[new Random().Next(0, guessSpace.Count)];
                        sb.Append("Random Guess");
                        predictionTrace = sb.ToString();
                        return randomGuess;
                    }
                    else
                    {
                        // Continue traversing the tree with the most common value of the same target
                        curNode = curNode.Children[mostCommonAttrVal];
                        sb.Append(curNode.SplittingAttributeName).Append("->");
                    }
                    /*/
                    string randomGuess = new Random().Next(1, 100000) < 50000 ? "True" : "False";
                    sb.Append("Random Guess");
                    predictionTrace = sb.ToString();
                    return randomGuess;
                    //*/
                }
                else
                {
                    curNode = curNode.Children[splittingAttr];
                    sb.Append(curNode.SplittingAttributeName).Append("->");
                }
            }
            sb.Append(curNode.Label);
            predictionTrace = sb.ToString();
            return curNode.Label;
        }

        private void ExerciseDecisionTree(int trueResultIdx)
        {
            int instanceCounts = 0;
            int correctPredictionCounts = 0;
            // Read in test data
            List<List<string>> instances = new List<List<string>>();
            using (StreamReader sr = File.OpenText(TEST_DATA_RELATIVE_PATH))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    if (s.StartsWith(ATTRIBUTE_ANNOTATION) || s.StartsWith(DATA_ANNOTATION) || string.IsNullOrEmpty(s))
                    {
                        continue;
                    }
                    List<string> newInstance = new List<string>(s.Split(','));
                    if (newInstance.Count == attributes.Count)
                    {
                        instanceCounts++;
                        instances.Add(newInstance);
                    }
                }
            }

            // Extra credit: Assign most common value of A among other examples with the same target value
            // Build a data structure that keeps track of the most common value of A for each target value 
            // for fast lookup while encountered with an unkonwn value during prediction phase.
            Dictionary<string, List<string>> mostCommonAttrValsForClass = ExtractMostCommonAttrValsForClass(trueResultIdx, examples);

            foreach (var instance in instances)
            {
                string predictionTrace = "";
                if (PredictOutcome(instance, trueResultIdx, mostCommonAttrValsForClass, out predictionTrace) == instance[trueResultIdx])
                {
                    correctPredictionCounts++;
                }
            }

            Console.WriteLine("Totoal Test Instance: {0}, Correctly Predicted: {1}, Accuracy Rate: {2}", instanceCounts, correctPredictionCounts, (double)correctPredictionCounts / instanceCounts);
        }

        private Dictionary<string, List<string>> ExtractMostCommonAttrValsForClass(int trueResultIdx, List<List<string>> instances)
        {
            int decisionAttrCount = attributes.Count - 1;
            var attrTallyByClass = new Dictionary<string, List<Dictionary<string, int>>>(attributes[trueResultIdx].AttributeVals.Count);
            foreach (var classVal in attributes[trueResultIdx].AttributeVals)
            {
                attrTallyByClass.Add(classVal, new List<Dictionary<string, int>>(decisionAttrCount));
                for (int i = 0; i < decisionAttrCount; i++)
                {
                    attrTallyByClass[classVal].Add(new Dictionary<string, int>());
                }
            }
            foreach (var instance in instances)
            {
                List<Dictionary<string, int>> attrTally = attrTallyByClass[instance[trueResultIdx]];
                for (int i = 0; i < decisionAttrCount; i++)
                {
                    if (attrTally[i].ContainsKey(instance[i]))
                    {
                        attrTally[i][instance[i]]++;
                    }
                    else
                    {
                        attrTally[i][instance[i]] = 1;
                    }
                }
            }
            var mostCommonAttrValsForClass = new Dictionary<string, List<string>>(attrTallyByClass.Count);
            foreach (var classTallyData in attrTallyByClass)
            {
                var classVal = classTallyData.Key;
                var attrTally = classTallyData.Value;
                mostCommonAttrValsForClass.Add(classVal, new List<string>(decisionAttrCount));
                for (int i = 0; i < decisionAttrCount; i++)
                {
                    mostCommonAttrValsForClass[classVal].Add("");
                }
                for (int i = 0; i < attrTally.Count; i++)
                {
                    Dictionary<string, int> tallyData = attrTally[i];
                    string mostCommonVal = "";
                    int mostCommonCount = -1;
                    foreach (var tallyEntry in tallyData)
                    {
                        if (tallyEntry.Value > mostCommonCount)
                        {
                            mostCommonVal = tallyEntry.Key;
                            mostCommonCount = tallyEntry.Value;
                        }
                    }
                    mostCommonAttrValsForClass[classVal][i] = mostCommonVal;
                }
            }

            return mostCommonAttrValsForClass;
        }

        public void Start()
        {
            Console.WriteLine("Reading Training Data...");
            ReadTrainingData();

            int classAttrIdx = (attributes.Count - 1);
            // In the given dataset, the class attr happens to be the attribute on the last index.
            HashSet<int> attrToTestIdxs = new HashSet<int>();
            for (int i = 0; i < attributes.Count - 1; i++)
            {
                // Exclude the class Attribute becuase it doesn't make sense to split the tree by it
                if (i != classAttrIdx)
                {
                    attrToTestIdxs.Add(i);
                }
            }

            Console.WriteLine("Building Decision Tree...");
            ID3(root, examples, classAttrIdx, attrToTestIdxs);

            Console.WriteLine("Running Against Test Data...");
            ExerciseDecisionTree(classAttrIdx);
        }

        private void primitivePrintTree()
        {
            Queue<DecisionTreeNode> q = new Queue<DecisionTreeNode>();
            q.Enqueue(root);
            int curLvlCount = 1;
            int nextLvlCount = 0;

            while(q.Count > 0)
            {
                DecisionTreeNode n = q.Dequeue();
                curLvlCount--;
                foreach (KeyValuePair<string, DecisionTreeNode> c in n.Children)
                {
                    q.Enqueue(c.Value);
                }
                nextLvlCount += n.Children.Count;
                Console.Write(n);

                if (curLvlCount == 0)
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    curLvlCount = nextLvlCount;
                    nextLvlCount = 0;
                }
            }
        }

        static void Main(string[] args)
        {
            try
            {
                List<double> thresholds = new List<double>() { 0.01, 0.05, 1.0};
                foreach (double th in thresholds)
                {
                    THRESHOLD = th;
                    Console.WriteLine("=== Test Run Threshold: {0} ===", th);
                    ClickstreamMining main = new ClickstreamMining();
                    main.Start();
                    Console.WriteLine("===============================");
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("Make sure the Input Data file is at the correct location in relation to the executable. See doc for details");
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }
    }
}
