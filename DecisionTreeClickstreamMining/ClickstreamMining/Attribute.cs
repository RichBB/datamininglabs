﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClickstreamMining
{
    public class Attribute
    {
        public string AttributeName { get; set; }
        public HashSet<string> AttributeVals { get; set; }

        public override bool Equals(object obj)
        {
            Attribute that = (Attribute)obj;
            return AttributeName.Equals(that.AttributeName) && AttributeVals.SetEquals(that.AttributeVals);
        }

        public override int GetHashCode()
        {
            return AttributeName.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach(string s in AttributeVals)
            {
                sb.Append(s).Append(",");
            }
            return string.Format("{0}->{1}", AttributeName, sb.ToString());
        }
    }
}
