﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaiveBayes
{
    class NaiveBayesDiabetesClassifier
    {
        public static string TRAINING_DATA_PATH_TEMPLATE = @"../../Data/SplitAttempt{0}/split{0}.diabetes.trainset";
        public static string TESTING_DATA_PATH_TEMPLATE = @"../../Data/SplitAttempt{0}/split{0}.diabetes.testset";

        public static string TRAINING_DATA_PATH = @"../../Data/diabetes_train.txt";
        public static string TESTING_DATA_PATH = @"../../Data/diabetes_test.txt";

        private static int CLASS_IDX = 8;
        private static string POSITIVE_CLASS = "1";

        private double trainingEntryCount = 0;
        private double negativeExampleCount = 0;
        private double positiveExampleCount = 0;
        private double pNegativeExample = 0.0;
        private double pPositiveExample = 0.0;
        private Dictionary<string, int> allData = new Dictionary<string, int>();
        private Dictionary<string, int> negativeData = new Dictionary<string, int>();
        private Dictionary<string, int> positiveData = new Dictionary<string, int>();
        private Dictionary<string, double> negativeConditionProb = new Dictionary<string, double>();
        private Dictionary<string, double> positiveConditionProb = new Dictionary<string, double>();

        private void AddToDictionaryHelper(IDictionary<string, int> targetDic, string word, int occuranceCount)
        {
            if (!targetDic.ContainsKey(word))
            {
                targetDic.Add(word, occuranceCount);
            }
            else
            {
                targetDic[word] = targetDic[word] + occuranceCount;
            }
        }

        private void ReadDataItemCount(string[] entryData, bool isPositive)
        {
            for (int i = 0; i < entryData.Length - 1; i++)
            {
                var curData = entryData[i];
                AddToDictionaryHelper(allData, curData, 1);
                if (isPositive)
                {
                    AddToDictionaryHelper(positiveData, curData, 1);
                }
                else
                {
                    AddToDictionaryHelper(negativeData, curData, 1);
                }
            }
        }

        private void ReadTrainingData()
        {
            using (StreamReader sr = File.OpenText(TRAINING_DATA_PATH))
            {
                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    trainingEntryCount++;
                    string[] entryData = line.Split(',');
                    bool isPositive = POSITIVE_CLASS.Equals(entryData[CLASS_IDX], StringComparison.CurrentCultureIgnoreCase);
                    if (isPositive)
                    {
                        positiveExampleCount++;
                    }
                    else
                    {
                        negativeExampleCount++;
                    }
                    ReadDataItemCount(entryData, isPositive);
                }
            }
        }

        private void CalculateTrainExampleClassProb()
        {
            if (trainingEntryCount > 0)
            {
                pNegativeExample = negativeExampleCount / trainingEntryCount;
                pPositiveExample = positiveExampleCount / trainingEntryCount;
            }
        }

        private void CalculateConditionProbsHelper(IDictionary<string, int> sourceDictionary, IDictionary<string, double> targetStorage)
        {
            double n = 0;
            foreach (var itemData in sourceDictionary)
            {
                n += itemData.Value;
            }
            foreach (var itemData in allData)
            {
                double denominator = (n + allData.Count);
                double numerator = 0;
                if (sourceDictionary.ContainsKey(itemData.Key))
                {
                    numerator += sourceDictionary[itemData.Key];
                }
                double itemConditionalProb = numerator / denominator;
                // Since that each conditional probability tend to be small,
                // use the Log of the probability to reduce the chance of having
                // overflow/underflow. Note that this means later when we're supposed to
                // multiply conditional prob of words, we will be using additions instead.
                itemConditionalProb = Math.Log10(itemConditionalProb);
                targetStorage.Add(itemData.Key, itemConditionalProb);
            }
        }

        private void CalculateConditionProbs()
        {
            CalculateConditionProbsHelper(positiveData, positiveConditionProb);
            CalculateConditionProbsHelper(negativeData, negativeConditionProb);
        }

        private void NaiveBayesCore()
        {
            double totalCount = 0;
            double correctPredictionCount = 0;
            int truePositive = 0;
            int trueNegative = 0;
            int falsePositive = 0;
            int falseNegative = 0;
            using (StreamReader sr = File.OpenText(TESTING_DATA_PATH))
            {
                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    totalCount++;
                    string[] entryData = line.Split(',');
                    bool isPositive = POSITIVE_CLASS.Equals(entryData[CLASS_IDX], StringComparison.CurrentCultureIgnoreCase);
                    double VnbPositive = pPositiveExample;
                    double VnbNegative = pNegativeExample;
                    for (int i = 0; i < entryData.Length - 1; i++)
                    {
                        // Since the probability are stored as the log10 of the real value, we use addition here
                        if (positiveConditionProb.ContainsKey(entryData[i]))
                        {
                            VnbPositive += positiveConditionProb[entryData[i]];
                        }
                        if (negativeConditionProb.ContainsKey(entryData[i]))
                        {
                            VnbNegative += negativeConditionProb[entryData[i]];
                        }
                    }
                    bool predictPositive = (VnbPositive > VnbNegative);
                    if (isPositive == predictPositive)
                    {
                        correctPredictionCount++;
                        if (predictPositive)
                        {
                            truePositive++;
                        }
                        else
                        {
                            trueNegative++;
                        }
                    }
                    else
                    {
                        if (predictPositive)
                        {
                            falsePositive++;
                        }
                        else
                        {
                            falseNegative++;
                        }
                    }
                }
            }
            Console.WriteLine("# Test Entries: {0}, # Correct Predictions: {1}, Accuracy Rate: {2}", totalCount, correctPredictionCount, correctPredictionCount/totalCount);
            Console.WriteLine("True Positive: {0}, True Negative: {1}", truePositive, trueNegative);
            Console.WriteLine("False Positive: {0}, False Negative: {1}", falsePositive, falseNegative);
        }

        public void Start()
        {
            Console.WriteLine("Learning Naive Bayes Classifier...");
            ReadTrainingData();
            CalculateTrainExampleClassProb();
            CalculateConditionProbs();
            Console.WriteLine("Classify With Naive Bayes...");
            NaiveBayesCore();
        }

        static void Main(string[] args)
        {
            try
            {
                /*
                for (int splitNum = 1; splitNum <= 5; splitNum++)
                {
                    Console.WriteLine("==== Running with Split Set #{0} ====", splitNum);
                    TRAINING_DATA_PATH = string.Format(TRAINING_DATA_PATH_TEMPLATE, splitNum);
                    TESTING_DATA_PATH = string.Format(TESTING_DATA_PATH_TEMPLATE, splitNum);
                    NaiveBayesDiabetesClassifier main = new NaiveBayesDiabetesClassifier();
                    main.Start();
                    Console.WriteLine("=====================================\n");
                }
                /*/
                NaiveBayesDiabetesClassifier main = new NaiveBayesDiabetesClassifier();
                main.Start();
                //*/
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("Make sure the training and testing data are in the correct directory in relation to the executable.");
                Console.WriteLine("See documentation for more details.");
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
